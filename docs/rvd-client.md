# Rivendell Client

The Rivendell Client is a tool that all Webits developers should use, in order to streamline and automate the development process. It it the main hub for working with multiple projects, vagrant boxes, themes and modules.

Takes care of the majority of the composer and bootstrap code and commands we usually do to be able to work on mupltiple projects. In addition, it can be used a tool to manage our customer (to a limited degree), as well as create in instances, database and virtual hosts on the servers.

Please see the [installation][ILIn] page in order to get going and [release page][ILRp] for the history and the currently supported features

## Feature set

### Vagrant management
You can (and should) use this tool, to make sure you are always working with the latest version of our vagrant boxes, as well as to be able to change boxes at will.
This tool takes care of all your webits issued VMs and makes sure that your local environment is up to date with our server setup, so you will be able to develop in real life conditions, avoiding issues that happen because some php version or module version is different between the server and your local machine.

### Customer management

### Project Management

### Environment management

### Instance management
