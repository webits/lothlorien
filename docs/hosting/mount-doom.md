# Hosting - Server Setup - MountDoom

OS : Debian 9

This is the server setup for our main server that is used internally.
This server holds the tools we need to implement CI and perform daily tasks:
* Satis : Our package manager
* PHP Censor : Our CI tool
* Lothlorien : Our documentation


## Basics Installation and configuration

The following could be included but could to have
```bash
apt install apt-transport-https lsb-release ca-certificates
wget -O /etc/apt/trusted.gpg.d/php.gpg https://packages.sury.org/php/apt.gpg
echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" > /etc/apt/sources.list.d/php.list
apt update && apt upgrade
apt install -y git zip unzip
```

### Install apache2
`apt install apache2`

#### Remove directory listings:

`sed -i "s/Options Indexes FollowSymLinks/Options FollowSymLinks/" /etc/apache2/apache2.conf`

#### Add http2 support:
```bash
a2enmod http2
nano /etc/apache2/conf-available/http2.conf
```

Fill in the following:
```bash
<IfModule mod_http2.c>
    Protocols h2 h2c http/1.1
</IfModule>
```

```bash
a2enconf http2
```

#### Add our default security
`nano /etc/apache2/conf-available/security-webits.conf`

Fill in the following:
```bash
# Security settings
# https://securityheaders.io/ and https://httpoxy.org/
<IfModule mod_headers.c>
  Header always set X-Content-Type-Options "nosniff"
  Header always set X-Frame-Options "sameorigin"
  Header always set X-Xss-Protection "1; mode=block"
  Header always set Referrer-Policy "strict-origin-when-cross-origin"
  RequestHeader unset Proxy early
</IfModule>

# SSL settings, see https://mozilla.github.io/server-side-tls/ssl-config-generator/?server=apache-2.4.25&openssl=1.1.0f&hsts=yes&profile=modern
<IfModule mod_ssl.c>
  SSLProtocol all -SSLv3 -TLSv1 -TLSv1.1
  SSLCipherSuite ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA256
  SSLHonorCipherOrder on
  SSLCompression off
  SSLUseStapling on
  SSLStaplingResponderTimeout 5
  SSLStaplingReturnResponderErrors off
  SSLStaplingCache shmcb:${APACHE_RUN_DIR}/ocsp_scache(128000)
  SSLSessionCache shmcb:${APACHE_RUN_DIR}/ssl_scache(512000)
  SSLSessionCacheTimeout 300
</IfModule>

# Prevent access to .bzr and .git directories and files.
<DirectoryMatch "/\.(bzr|git)">
  Require all denied
</DirectoryMatch>

# Prevent access do some stanard application txt files.
<FilesMatch "^(CHANGELOG|COPYRIGHT|INSTALL|INSTALL\.mysql|INSTALL\.pgsql|INSTALL\.sqlite|LICENSE|MAINTAINERS|UPGRADE|README)\.txt">
  Require all denied
</FilesMatch>

# Security setting for config folder in our apps.
<DirectoryMatch "^/var/www/html/.*/(private|config|sync|translations|twig)">
  <IfModule mod_authz_core.c>
    Require all denied
  </IfModule>

  # Deny all requests from Apache 2.0-2.2.
  <IfModule !mod_authz_core.c>
    Deny from all
  </IfModule>
  # Turn off all options we don't need.
  Options -Indexes -ExecCGI -Includes -MultiViews

  # If we know how to do it safely, disable the PHP engine entirely.
  <IfModule mod_php.c>
    php_flag engine off
  </IfModule>
</DirectoryMatch>
```
`a2enconf security-webits`

#### Add apache user azaghal
```bash
adduser azaghal
usermod -aG www-data azaghal
usermod -aG sudo azaghal
chown -R azaghal:www-data /var/www/html
cd ~/
ssh-keygen -t rsa -C "hosting@webits.gr"
```

#### Add the server pages
As root user:
```bash
cd /var/www/html
wget https://raw.githubusercontent.com/AndiDittrich/HttpErrorPages/master/dist/pages.tar
mv dist error_pages
chown -R azaghal:www-data /var/www/html
nano /etc/apache2/conf-enabled/webits.conf
```

add the following:

```bash
ErrorDocument 400 /error_pages/HTTP400.html
ErrorDocument 401 /error_pages/HTTP401.html
ErrorDocument 403 /error_pages/HTTP403.html
ErrorDocument 404 /error_pages/HTTP404.html
ErrorDocument 500 /error_pages/HTTP500.html
ErrorDocument 501 /error_pages/HTTP501.html
ErrorDocument 502 /error_pages/HTTP502.html
ErrorDocument 503 /error_pages/HTTP503.html
```

`systemctl restart apache2`

#### Add the server root vhost
`nano /etc/apache2/sites-available/server.conf`

Add the following:

```bash
<VirtualHost *:80>
        ServerAdmin giorgos@webits.gr
        ServerName  server-mtdoom.webits.gr

        # Indexes + Directory Root.
        DirectoryIndex index.html
        DocumentRoot /var/www/html
</VirtualHost>
```

```bash
a2ensite server.conf
systemctl reload apache2
```

Now add the certbot certificate for the server:

`certbot --apache`

### Install php7.2
```bash
apt install -y php7.2-fpm php7.2-xml php7.2-mbstring php7.2-zip php7.2-curl php7.2-mysql 
a2enmod proxy_fcgi setenvif
a2enconf php7.2-fpm
```

#### Configure php-fpm
`nano /etc/php/7.2/fpm/pool.d/www.conf`

Fill in the following in their respective fields:
``` bash
listen = /run/php/php-fpm.sock
pm = dynamic
pm.max_children = 5
pm.start_servers = 2
pm.min_spare_servers = 1
pm.max_spare_servers = 6
pm.max_requests = 5000
```

#### Set ServerName
`nano /etc/apache2/conf-available/webits.conf`

Fill in the following:
```bash
ServerName <Hetzner_name>
```

`a2enconf webits` 

### Install mariadb
`apt install mariadb-server`

#### Configure MariaDB
`nano /etc/mysql/conf.d/mysql.cnf`

Fill in the following:
```bash
[mysql]
[mysqld]
# Set character set and collation to utf8mb4.
character_set_server = utf8mb4
collation_server = utf8mb4_unicode_ci

# Common Configuration
skip_name_resolve = 1
connect_timeout = 10
interactive_timeout = 25
wait_timeout = 60
max_allowed_packet = 64M
table_open_cache = 2000
table_definition_cache =  2000
thread_handling = pool-of-threads

# Slow Log Configuration
slow_query_log = 1
slow_query_log_file = /var/log/mysql/mysql_slow_query.log
long_query_time = 5
#log_queries_not_using_indexes = 1

# InnoDB Configuration
innodb_buffer_pool_size = 256M
innodb_flush_method = O_DIRECT
innodb_file_per_table = 1
innodb_flush_log_at_trx_commit = 2
innodb_large_prefix = 1
innodb_file_format = barracuda
```

#### Add the remote db user
```mysql
mariadb -uroot
MariaDB > CREATE USER 'durin'@'%';
MariaDB > GRANT ALL PRIVILEGES ON *.* TO 'durin'@'%' IDENTIFIED BY <our mysql password>;
MariaDB > exit;
```

### Install Redis <- Maybe not needed for our infra
apt install redis-server
nano /etc/redis/redis.conf
```
...
supervised systemd
...
```

// Create the server redis instance mapper
su azaghal
cd ~
mkdir config
nano config/redis.yml
```
redis:
	database: 0
		instance: h1013
```

### Install composer globally
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php -r "if (hash_file('SHA384', 'composer-setup.php') === '669656bab3166a7aff8a7506b8cb2d1c292f042046c5a994c43155c0be6190fa0355160742ab2e1c88d40d5be660b410') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"The first command will grab the Composer installer as a PHAR (PHP Archive) file, while the second ensures that the installer is free from any errors or corruption. After executing these commands, you will have the latest build of Composer’s installer on your drive.
php composer-setup.php --install-dir=/usr/local/bin --filename=composer
rm composer-setup.php

### CertBot
```bash
apt-get install python-certbot-apache -t stretch-backports
certbot --apache
```

### Restarting our services
```bash
systemctl restart apache2
systemctl restart php7.2-fpm
```

## Infra apps installation and configuration

This section describrs how to install the required external software.

### Add the required authentication data
```bash
su azaghal
mkdir ~/.composer
nano ~/.composer/auth.json
```

Add the following:
```json
{
    "http-basic": {},
    "bitbucket-oauth": {
        "bitbucket.org": {
            "consumer-key": "SxAf5mAyeCqMvyxcj3",
            "consumer-secret": "CZA4AK4r3TCJfpHb7UwgvJzGyEMw8ert",
            "access-token": "c_9IpUKGXsq0yxQflcTIrP4bKIFTkLVI3pdlCcoDCZzNNUr9OdvsBxZGdeAuglKvPTOZKAYggoRDEzUSjSk=",
            "access-token-expiration": 1538341270
        }
    }
}
```

### Satis
```
cd /var/www/html/
su azaghal
composer create-project composer/satis:dev-master
cd satis
nano satis.json
```

Fill in the following:
```json
{
  "name": "Webits Packages Repository",
  "homepage": "https://packages.webits.gr",
  "repositories": [
    { "type": "vcs", "url": "<https links to repo you want to track>"}
  ],
  "require-all": true,
  "output-dir": "public",
  "archive": {
    "directory": "dist",
    "format": "zip",
    "skip-dev": false
  }
}
```

#### Add the satis vhost
You need to be the root user:
`nano /etc/apache2/sites-available/packages_webits_gr.conf`

Fill in the following:
```bash
<VirtualHost *:80>
        ServerAdmin giorgos@webits.gr
        ServerName  packages.webits.gr

        DirectoryIndex index.html
        DocumentRoot /var/www/html/satis/public/
</VirtualHost>
```


```bash
a2ensite packages_webits_gr
systemctl reload apache2
su azaghal
php bin/satis build
crontab -e
*/3 * * * * php /var/www/html/satis/bin/satis build
certbot --apache (as root)
```

#### Add the satis authentication

Verify the conf files for the site and reload apache.
```bash
cd /var/www/html/satis/public
htpasswd -c ./.htpasswd composer (use e54qcV)
nano /etc/apache/sites-enabled/packages_webits_gr-le-ssl.conf
```

Add the following:
```bash
<Directory "/var/www/html/satis/public">
        AuthType Basic
        AuthName "Restricted Content"
        AuthUserFile /var/www/html/satis/public/.htpasswd
        Require valid-user
</Directory>
```

`systemctl restart apache2`

#### Mkdocs for Lothlorien
```bash
su azaghal
cd ~
wget https://bootstrap.pypa.io/get-pip.py
sudo python get-pip.py
sudo pip install --upgrade pip
sudo pip install mkdocs
sudo pip install mkdocs-material
```
### Rivendell Server Endpoint

