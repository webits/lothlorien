# Hosting - Server Setup - Wordpress

OS : Debian 9

## Basics Installation and configuration
Add the following to ~/.bashrc
```bash
# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=999999
HISTFILESIZE=999999

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# If set, the pattern "**" used in a pathname expansion context will
# match all files and zero or more directories and subdirectories.
#shopt -s globstar

# make less more friendly for non-text input files, see lesspipe(1)
#[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "${debian_chroot:-}" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
    xterm-color|*-256color) color_prompt=yes;;
esac

# uncomment for a colored prompt, if the terminal has the capability; turned
# off by default to not distract the user: the focus in a terminal window
# should be on the output of commands, not on the prompt
force_color_prompt=yes

if [ -n "$force_color_prompt" ]; then
    if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
        color_prompt=yes
    else
        color_prompt=
    fi
fi

if [ "$color_prompt" = yes ]; then
    #PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
    PS1='${debian_chroot:+($debian_chroot)}\[\033[01;31m\]\u\[\033[01;33m\]@\[\033[01;36m\]\h \[\033[01;33m\]\w \[\033[01;35m\]\$ \[\033[00m\]'      else
    PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
fi
unset color_prompt force_color_prompt

# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*|rxvt*)
    PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"
    ;;
*)
    ;;
esac

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    #alias dir='dir --color=auto'
    #alias vdir='vdir --color=auto'

    #alias grep='grep --color=auto'
    #alias fgrep='fgrep --color=auto'
    #alias egrep='egrep --color=auto'
fi

# colored GCC warnings and errors
#export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'

# some more ls aliases
#alias ll='ls -l'
#alias la='ls -A'
#alias l='ls -CF'

# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.

if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi
```

The following could be included but could to have
```bash
apt install apt-transport-https lsb-release ca-certificates
apt update && apt upgrade
apt install -y git zip unzip
```

### Install apache2
`apt install apache2`

#### Remove directory listings:

`sed -i "s/Options Indexes FollowSymLinks/Options FollowSymLinks/" /etc/apache2/apache2.conf`

#### Add http2 support:
```bash
a2enmod http2
nano /etc/apache2/conf-available/http2.conf
```

Fill in the following:
```bash
<IfModule mod_http2.c>
    Protocols h2 h2c http/1.1
</IfModule>
```

```bash
a2enconf http2
a2enmod proxy_fcgi setenvif
a2enconf php*-fpm
a2dismod php*
a2dismod mpm_prefork
a2enmod mpm_event
systemctl restart apache2
```

#### Add our default security
`nano /etc/apache2/conf-available/security-webits.conf`

Fill in the following:
```bash
# Security settings
# https://securityheaders.io/ and https://httpoxy.org/
<IfModule mod_headers.c>
  Header always set X-Content-Type-Options "nosniff"
  Header always set X-Frame-Options "sameorigin"
  Header always set X-Xss-Protection "1; mode=block"
  Header always set Referrer-Policy "strict-origin-when-cross-origin"
  RequestHeader unset Proxy early
</IfModule>

# SSL settings, see https://mozilla.github.io/server-side-tls/ssl-config-generator/?server=apache-2.4.25&openssl=1.1.0f&hsts=yes&profile=modern
<IfModule mod_ssl.c>
  SSLProtocol all -SSLv3 -TLSv1 -TLSv1.1
  SSLCipherSuite ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA256
  SSLHonorCipherOrder on
  SSLCompression off
  SSLUseStapling on
  SSLStaplingResponderTimeout 5
  SSLStaplingReturnResponderErrors off
  SSLStaplingCache shmcb:${APACHE_RUN_DIR}/ocsp_scache(128000)
  SSLSessionCache shmcb:${APACHE_RUN_DIR}/ssl_scache(512000)
  SSLSessionCacheTimeout 300
</IfModule>

# Prevent access to .bzr and .git directories and files.
<DirectoryMatch "/\.(bzr|git)">
  Require all denied
</DirectoryMatch>

# Prevent access do some stanard application txt files.
<FilesMatch "^(CHANGELOG|COPYRIGHT|INSTALL|INSTALL\.mysql|INSTALL\.pgsql|INSTALL\.sqlite|LICENSE|MAINTAINERS|UPGRADE|README)\.txt">
  Require all denied
</FilesMatch>

# Security setting for config folder in our apps.
<DirectoryMatch "^/var/www/html/.*/(private|config|sync|translations|twig)">
  <IfModule mod_authz_core.c>
    Require all denied
  </IfModule>

  # Deny all requests from Apache 2.0-2.2.
  <IfModule !mod_authz_core.c>
    Deny from all
  </IfModule>
  # Turn off all options we don't need.
  Options -Indexes -ExecCGI -Includes -MultiViews

  # If we know how to do it safely, disable the PHP engine entirely.
  <IfModule mod_php.c>
    php_flag engine off
  </IfModule>
</DirectoryMatch>
```
`a2enconf security-webits`

#### Add apache user azaghal
Add no password so the remote ssh will work automatically
```bash
adduser azaghal
usermod -aG www-data azaghal
usermod -aG sudo azaghal
chown -R azaghal:www-data /var/www/html
```

At this point add the azaghal-remote key to the authorized users
```bash
nano ~/.ssh/authorized_keys

# Azaghal from webits developers
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDLbZ06QNYw1Z+EwfevL1WegS5yxawcM1wIMeTvn9FBSsr8ZK9iZzmc76ng/Z/Qr562VHXuZ7VmLePj4Qek54Hh8qiOsW1hO3rKybkzAMwpYJM0gektoq2WLIFwB/SDaBxDTBvdUeB5cMsyEdrwpo/svz5INZVysxSqHabIWVXDtA8dCIfG1VesO6e3fW50yaV4Xq46IfCd/tb/GT5pXPVqvLzhuy7Z3JFPhtQZ0TKLnfLGt03v3G/w01MuJZz3Lnkw9n/v3+4aPujU2eWCkYlmYku8l8tKkY0kQclKnlGfMDftAav7tCGARuVoKQMXkk6rljKoPc6Tc39q4av+zK9p hosting@webits.gr
```

We also need to allos the sudo chown without password for this user
```bash
nano /etc/sudoers

# Add to the end
azaghal ALL = NOPASSWD: /usr/bin/chown
```

#### Add the server pages
As root user:
```bash
cd /var/www/html
wget https://raw.githubusercontent.com/AndiDittrich/HttpErrorPages/master/dist/pages.tar
tar -xf pages.tar
rm pages.tar
mv dist error_pages
chown -R azaghal:www-data /var/www/html
nano /etc/apache2/conf-available/webits.conf
```

add the following:

```bash
ErrorDocument 400 /error_pages/HTTP400.html
ErrorDocument 401 /error_pages/HTTP401.html
ErrorDocument 403 /error_pages/HTTP403.html
ErrorDocument 404 /error_pages/HTTP404.html
ErrorDocument 500 /error_pages/HTTP500.html
ErrorDocument 501 /error_pages/HTTP501.html
ErrorDocument 502 /error_pages/HTTP502.html
ErrorDocument 503 /error_pages/HTTP503.html
```

`systemctl restart apache2`

#### Add the server root vhost
`nano /etc/apache2/sites-available/server.conf`

Add the following:

```bash
<VirtualHost *:80>
        ServerAdmin giorgos@webits.gr
        ServerName  server-<hetzner-name>.webits.gr

        # Indexes + Directory Root.
        DirectoryIndex index.html
        DocumentRoot /var/www/html
</VirtualHost>
```

```bash
a2ensite server.conf
systemctl reload apache2
```

Now point the DNS correctly to the server and add the certbot certificate for the server:
### CertBot
```bash
apt install certbot python-certbot-apache
certbot --apache
```

### Install php7.3
```bash
wget -O /etc/apt/trusted.gpg.d/php.gpg https://packages.sury.org/php/apt.gpg
echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" > /etc/apt/sources.list.d/php.list
apt update && apt upgrade -y
apt install -y php7.3-fpm php7.3-xml php7.3-mbstring php7.3-zip php7.3-curl php7.3-mysql 
a2enmod proxy_fcgi setenvif
a2enconf php7.3-fpm
```

#### Configure php-fpm
`nano /etc/php/7.3/fpm/pool.d/www.conf`

Fill in the following in their respective fields (or leave the defaults, should be fine):
``` bash
listen = /run/php/php7.*-fpm.sock
pm = dynamic
pm.max_children = 5
pm.start_servers = 2
pm.min_spare_servers = 1
pm.max_spare_servers = 6
pm.max_requests = 5000
```

#### Set ServerName and default modules
`nano /etc/apache2/conf-available/webits.conf`

Fill in the following:
```bash
ServerName Webits_<Hetzner_name>

<IfModule mod_mime.c>
        AddType application/javascript          js
        AddType application/vnd.ms-fontobject   eot
        AddType application/x-font-ttf          ttf ttc
        AddType font/opentype                   otf
        AddType application/x-font-woff         woff woff2
        AddType image/svg+xml                   svg svgz
        AddEncoding gzip                        svgz
</IfModule>

<IfModule mod_deflate.c>
        AddOutputFilterByType DEFLATE text/html text/plain text/css application/json
        AddOutputFilterByType DEFLATE application/javascript
        AddOutputFilterByType DEFLATE text/xml application/xml text/x-component
        AddOutputFilterByType DEFLATE application/xhtml+xml application/rss+xml application/atom+xml
        AddOutputFilterByType DEFLATE image/x-icon image/svg+xml application/vnd.ms-fontobject application/x-font-ttf font/opentype
</IfModule>

<ifModule mod_gzip.c>
        mod_gzip_on Yes
        mod_gzip_dechunk Yes
        mod_gzip_item_include file .(html?|txt|css|js|php|pl)$
        mod_gzip_item_include handler ^cgi-script$
        mod_gzip_item_include mime ^text/.*
        mod_gzip_item_include mime ^application/x-javascript.*
        mod_gzip_item_exclude mime ^image/.*
        mod_gzip_item_exclude rspheader ^Content-Encoding:.*gzip.*
</ifModule>

<IfModule mod_expires.c>
        ExpiresActive On
        ExpiresByType text/css "access plus 1 month"
        ExpiresByType text/javascript "access plus 1 month"
        ExpiresByType image/jpg "access plus 1 month"
        ExpiresByType image/gif "access plus 1 month"
        ExpiresByType image/jpeg "access plus 1 month"
        ExpiresByType image/png "access plus 1 month"
        ExpiresByType application/javascript "access plus 1 month"
        ExpiresByType application/x-shockwave-flash "access plus 1 month"
        ExpiresByType image/ico "access plus 1 month"
        ExpiresByType image/x-icon "access plus 1 month"
        ExpiresByType text/html "access plus 600 seconds"
        ExpiresDefault "access plus 2 days"
</IfModule>

#<ifModule mod_headers.c>
#  <filesMatch "\.(ico|pdf|flv|jpg|jpeg|png|gif|swf)$">
#    Header set Cache-Control "max-age=2592000, public"
#  </filesMatch>
#  <filesMatch "\.(css)$">
#    Header set Cache-Control "max-age=604800, public"
#  </filesMatch>
#  <filesMatch "\.(js)$">
#    Header set Cache-Control "max-age=216000, private"
#  </filesMatch>
#  <filesMatch "\.(xml|txt)$">
#    Header set Cache-Control "max-age=216000, public, must-revalidate"
#  </filesMatch>
#  <filesMatch "\.(html|htm|php)$">
#    Header set Cache-Control "max-age=1, private, must-revalidate"
#  </filesMatch>
#</ifModule>

<IfModule mod_cache.c>
    CacheQuickHandler off

    CacheIgnoreNoLastMod On
    CacheDefaultExpire 7200

    CacheIgnoreCacheControl On
    CacheLastModifiedFactor 0.5
    CacheIgnoreHeaders Set-Cookie Cookie
    CacheHeader on
    CacheLock on
    CacheDisable /wp-admin
    CacheDisable /wp-login.php
    CacheDisable /wp-cron.php

    SetOutputFilter CACHE
    AddOutputFilterByType DEFLATE text/html text/plain text/css application/javascript application/rss+xml text/xml image/svg+xml

    # The following line could be required or not depending on your Apache installation
    #LoadModule cache_disk_module modules/mod_cache_disk.so

    #<IfModule mod_cache_disk.c>
    #    CacheRoot /var/cache/apache2/mod_cache_disk
    #    CacheEnable disk /
    #    CacheDirLevels 2
    #    CacheDirLength 1
    #    CacheMaxFileSize 2000000
    #</IfModule>
</IfModule>
```

`a2enconf webits` 

### Install mariadb
`apt install -y mariadb-server`

#### Configure MariaDB
`nano /etc/mysql/conf.d/mysql.cnf`

Fill in the following:
```bash
[mysql]
[mysqld]
# Set character set and collation to utf8mb4.
character_set_server = utf8mb4
collation_server = utf8mb4_unicode_ci

# Common Configuration
skip_name_resolve = 1
connect_timeout = 10
interactive_timeout = 25
wait_timeout = 60
max_allowed_packet = 64M
table_open_cache = 2000
table_definition_cache =  2000
thread_handling = pool-of-threads

# Slow Log Configuration
slow_query_log = 1
slow_query_log_file = /var/log/mysql/mysql_slow_query.log
long_query_time = 5
#log_queries_not_using_indexes = 1

# InnoDB Configuration
innodb_buffer_pool_size = 256M
innodb_flush_method = O_DIRECT
innodb_file_per_table = 1
innodb_flush_log_at_trx_commit = 2
innodb_large_prefix = 1
innodb_file_format = barracuda
```

#### Add the remote db user
```mysql
mariadb -uroot
MariaDB > CREATE USER 'durin'@'%';
MariaDB > GRANT ALL PRIVILEGES ON *.* TO 'durin'@'%' IDENTIFIED BY <our mysql password>;
MariaDB > exit;
```

### Install Redis
apt install -y redis-server
nano /etc/redis/redis.conf
```
...
supervised systemd
...
```

// Create the server redis instance mapper
su azaghal
cd ~
mkdir config
nano config/redis.yml
```
redis:
    database: 0
        instance: h1013
```

### Install composer globally
```bash
cd ~
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php -r "if (hash_file('SHA384', 'composer-setup.php') === '669656bab3166a7aff8a7506b8cb2d1c292f042046c5a994c43155c0be6190fa0355160742ab2e1c88d40d5be660b410') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"The first command will grab the Composer installer as a PHAR (PHP Archive) file, while the second ensures that the installer is free from any errors or corruption. After executing these commands, you will have the latest build of Composer�s installer on your drive.
php composer-setup.php --install-dir=/usr/local/bin --filename=composer
rm composer-setup.php
```

### Restarting our services
```bash
systemctl restart apache2
systemctl restart php7.3-fpm
```
