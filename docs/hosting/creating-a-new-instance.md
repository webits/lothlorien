# Hosting - Common - Creating a new instance : wordpress

## Get the instance from Rivendell

## Create the instance on the server
```bash
su azaghal
cd /var/www/html
mkdir <instance_code>
cd <instance_code>
git clone <project_repo> .
cd ../
chown -R azaghal:www-data ./
chmod 700 rvd-instance/ patches/
composer install
```

## Create a database
```Mysql
exit (from user)
mysql -uroot
CREATE DATABASE w0002 CHARACTER SET = "utf8mb4" COLLATE = "utf8mb4_unicode_ci";
CREATE USER 'w0002'@'localhost' IDENTIFIED BY '7af7b9045c70bde29cf1e67cf41afa0b';
GRANT ALL ON w0002.* to 'w0002'@'localhost'
```

//Limit access to resources in vhosts
# Only allow access to cron.php etc. from localhost
<FilesMatch "^(cron|install|update|xmlrpc)\.php">
  Require local
</FilesMatch>